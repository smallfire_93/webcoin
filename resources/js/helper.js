var helper  = new Helper ();

function Helper(){
  var methods = this;
  var wrapper = $('.wrapper');
  var init =  function(){
    methods.scrollMenuSticky();
    methods.scrollUp();
    methods.stickyInParentMenu();
    methods.scrollTop();
    methods.countDown();
    methods.initSlider();
  }

  // fixed menu when srcoll
  methods.scrollMenuSticky=function(){
    var stickyTop = $(".sticky").offset().top;
    $(window).scroll(function(){
      var sticky = $('.sticky'),
      scroll = $(window).scrollTop();
      if (scroll > stickyTop){
        sticky.addClass('menu-fixed');
        // cart.addClass('show');
      }
      else{
        sticky.removeClass('menu-fixed');
        // cart.removeClass('show');
      }

      if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
      } else {
        $('.scrollup').fadeOut();
      }
    });
    $(window).on('load resize', function(event) {
      $('.page-content').css('min-height', $('body').height()  - $('.bg-breakcrumb').height() - $('#navigation').height() - $('header').height() - $('footer').height());
    });
  }
  // display button scroll top
  methods.scrollUp = function(){
    $('.scrollup').click(function () {
      $("html, body").animate({
        scrollTop: 0
      }, 600);
      return false;
    });
  }
  methods.stickyInParentMenu = function(){
    // $(".header .navbar-nav li a").stick_in_parent();
  }
  methods.scrollTop = function(){
    $('.header ').on('click','.navbar-nav li a', function(){
      var header = $('.header').height();
      var isFixed = $('.header').hasClass('menu-fixed');
      var offset =   isFixed ? 150  : 150  + header ;
      $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top - offset
      }, 500);
      return false;
    });
  }
  methods.countDown = function(){
    setInterval(function time(){
      var endTime = new Date("April 2, 2018 12:00:00 PDT");
      var endTime = (Date.parse(endTime)) / 1000;
      var now = new Date();
      var now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400);
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }
      $("#days").html(days);
      $("#hours").html(hours);
      $("#minutes").html(minutes);
      $("#seconds").html(seconds);
    }, 1000);

  }
  // plugin sider
  methods.initSlider = function(){

          $('.slider-benefits').slick({
            infinite: true,
            slidesToShow:3,
            slidesToScroll: 1,
            autoplay:true,
             speed: 300,
            responsive: [{
                breakpoint:768,
                settings: {
                  slidesToShow: 2,
                }
              },  {
                breakpoint: 420,
                settings: {
                  slidesToShow: 1,
                }
              },
            ]
          });
  }
  // call back init function
  init();
  return this;
}
